"""Exceptions."""

# pylint: disable=unused-import
from dump2polarion.exceptions import Dump2PolarionException, NothingToDoException  # noqa


class TestcasesException(Dump2PolarionException):
    """Testcases exception."""
