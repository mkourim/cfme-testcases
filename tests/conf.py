# pylint: disable=missing-docstring,redefined-outer-name,no-self-use

import os

DATA_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")
